package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh"
	"golang.org/x/term"
)

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	var err error
	var user string
	var pass string
	var cmds string
	var mgmt_addr string
	var timeout int = 10
	var sleep float32 = 2.0
	var verbose bool = false
	var logF *os.File
	var log bool = false
	var logfile string = fmt.Sprintf("%s_sshCLI.log", time.Now().Format("2006-01-02_15.04.05"))

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "--help" {
			fmt.Print("--user=username\tUsername\n")
			fmt.Print("--pass=password\tPassword, leave blank (i.e. --pass=) for interactive password input\n")
			fmt.Print("--verbose\tTurn on verbose output as STDERR, will show password in clear text\n")
			fmt.Printf("--timeout=INT\tTimeout integer in seconds, default is %d if ommited\n", timeout)
			fmt.Printf("--sleep=INT\tTime to sleep to wait for each command to complete, default is %f if ommitied\n", sleep)
			fmt.Print("--cmds=filename\tFile of commands to send to the remove device\n")
			fmt.Print("--log\t\tTurn on logging of STDOUT\n")
			fmt.Print("W.X.Y.Z\t\tManagement Address to connect to, defaults to port 22\n")
			fmt.Print("W.X.Y.Z:AB\tManagement Address to connect to on port AB\n")
			fmt.Print("--help\t\tPrint this help message\n")
			return
		}

		if arg == "--verbose" {
			verbose = true
		}

		if arg == "--pass=" {
			fmt.Printf("Password: ")
			bytepass, err := term.ReadPassword(int(syscall.Stdin))
			er(err)
			pass = string(bytepass)
		}

		regex := regexp.MustCompile(`--user=(\S+)`)
		if regex.MatchString(arg) {
			user = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--pass=(\S+)`)
		if regex.MatchString(arg) {
			pass = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--timeout=(-?\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseInt(regex.FindStringSubmatch(arg)[1], 10, 64)
			er(err)
			timeout = int(tmp)
		}

		regex = regexp.MustCompile(`--sleep=(\d+\.?\d*)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.ParseFloat(regex.FindStringSubmatch(arg)[1], 64)
			er(err)
			sleep = float32(tmp)
		}

		regex = regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+$`)
		if regex.MatchString(arg) {
			mgmt_addr = arg + ":22"
		}

		regex = regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+:\d+$`)
		if regex.MatchString(arg) {
			mgmt_addr = arg
		}

		regex = regexp.MustCompile(`--cmds=(\S+)`)
		if regex.MatchString(arg) {
			cmds = regex.FindStringSubmatch(arg)[1]
		}

		if arg == "--log" {
			log = true
			logF, err = os.Create(logfile)
			er(err)
			defer logF.Close()
		}

	}

	if user == "" {
		er(fmt.Errorf("username is not set, use: --user="))
	}

	if pass == "" {
		er(fmt.Errorf("password is not set, use: --pass="))
	}

	if mgmt_addr == "" {
		er(fmt.Errorf("management IP address not set, use: ww.xx.yy.zz or ww.xx.yy.zz:ab"))
	}

	if cmds == "" {
		er(fmt.Errorf("command file not set, use: --cmds="))
	}

	if verbose {
		fmt.Fprintf(os.Stderr, "IP = %s\n", mgmt_addr)
		fmt.Fprintf(os.Stderr, "user = %s\n", user)
		fmt.Fprintf(os.Stderr, "pass = %s\n", pass)
		fmt.Fprintf(os.Stderr, "cmds = %s\n", cmds)
		fmt.Fprintf(os.Stderr, "sleep = %f\n", sleep)
		fmt.Fprintf(os.Stderr, "timeout = %d\n\n", timeout)

		fmt.Fprint(os.Stderr, "Creating SSH connection\n")
	}
	sshConn, err := ssh.Dial("tcp", mgmt_addr, &ssh.ClientConfig{
		User:            user,
		Auth:            []ssh.AuthMethod{ssh.Password(pass)},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Timeout:         time.Duration(timeout) * time.Second,
	})
	er(err)

	session, err := sshConn.NewSession()
	er(err)

	if verbose {
		fmt.Fprint(os.Stderr, "SSH session established\n")
	}

	modes := ssh.TerminalModes{
		ssh.ECHO:          0,
		ssh.TTY_OP_ISPEED: 14400,
		ssh.TTY_OP_OSPEED: 14400,
	}

	if verbose {
		fmt.Fprint(os.Stderr, "Requesting terminal\n")
	}
	err = session.RequestPty("xterm", 80, 40, modes)
	er(err)

	if verbose {
		fmt.Fprint(os.Stderr, "Terminal granted\n")
	}

	if verbose {
		fmt.Fprint(os.Stderr, "Creating pipe to STDOUT\n")
	}
	stdBuf, err := session.StdoutPipe()
	er(err)

	if verbose {
		fmt.Fprint(os.Stderr, "Creating pipe to STDIN\n")
	}
	stdinBuf, err := session.StdinPipe()
	er(err)

	if verbose {
		fmt.Fprint(os.Stderr, "Starting shell\n")
	}
	err = session.Shell()
	er(err)

	if verbose {
		fmt.Fprintf(os.Stderr, "Opening command file: %s\n", cmds)
	}
	cmd_file, err := os.Open(cmds)
	er(err)
	defer cmd_file.Close()

	scanner := bufio.NewScanner(cmd_file)

	if verbose {
		fmt.Fprintf(os.Stderr, "Reading %s into memory\n", cmds)
	}

	for scanner.Scan() {
		if verbose {
			fmt.Fprintf(os.Stderr, "Writing \"%s\" to STDIN Buffer\n", scanner.Text())
		}
		stdinBuf.Write([]byte(scanner.Text() + "\n"))
		stdoutBuf := make([]byte, 1000000)
		if verbose {
			fmt.Fprintf(os.Stderr, "Sleeping for %f seconds\n", sleep)
		}
		time.Sleep(time.Duration(sleep) * time.Second)
		byteCount, err := stdBuf.Read(stdoutBuf)
		er(err)
		fmt.Printf("%s\n", string(stdoutBuf[:byteCount]))
		if log {
			logF.WriteString(string(stdoutBuf[:byteCount]))
		}
	}

	defer sshConn.Close()

}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
